# Fonctionnalités développées
Toutes les fonctionnalités obligatoires du sujet sont implémentées. De plus, la
classe abstraite `Character` permet de faciliter l'implémentation d'éléments
optionnels du sujet, comme la classe `Wise`. Si c'est autorisé, on pourrait donc
implémenter des fonctionnalités optionnelles entre le premier et le second
rendu.

# Statut des fonctionnalités implémentées
## Bugs connus
Il n'y a pas de bugs connus.

## Sources d'attention particulière
Quand un `Occupant` d'une case donnée refuse l'accès à un `ActiveCharacter`, sa
méthode `process` doit appeller la méthode `playAgain` de l'interface
`ActiveCharacter`. De nombreux bugs rencontrés au cours du développement
venaient du fait que la méthode `playAgain` était appellée pour que le
personnage rejoue, mais ce dernier rejouait le même mouvement, donnant naissance
à une boucle infinie.

Tous les bugs connus ont été corrigés, mais on sait que les méthodes `process`
sont particulièrement susceptibles de contenir des problèmes.

