import java.util.ArrayList;
import java.util.List;

public class Wall  {

    private ArrayList<Stone> wall;
    private Position[] sides;

    public Wall(ArrayList<Stone> wall) {
        Position posA = wall.get(0).getPos();
        Position posB = wall.get(wall.size() - 1).getPos();
        // v is the vector from the stone #0 to the stone #1
        int vx = wall.get(1).getPos().getX() - posA.getX();
        int vy = wall.get(1).getPos().getY() - posA.getY();
        sides = new Position[2];
        sides[0] = new Position(posA.getX() - vx, posA.getY() - vy);
        sides[1] = new Position(posB.getX() + vx, posB.getY() + vy);

        this.wall = wall;
        for (Stone i: wall) {
            i.setWall(this);
        }
    }

    /**
     * Give the position of the stone block that is on the first side of the wall
     * @return the position of the first side of the wall
     */
    public Position sideA() {
        return sides[0];
    }

    /**
     * Give the position of the stone block that is on the last side of the wall
     * @return the position of the last side of the wall
     */
    public Position sideB() {
        return sides[1];
    }

    /**
     * Gives access to the stones in the wall
     * @return a list of stones
    */
    public List<Stone> getStones() {
        return wall;
    }
}
