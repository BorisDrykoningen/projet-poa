public class Tool extends Occupant{
    /**
     * Creates a new Occupant
     *
     * @param pos     the occupant's position
     * @param board   the game's board
     */
    public Tool(Position pos, Board board) {
        super(pos, board, '@');
    }

    /**
     * If they don't have one already, gives the character a tool to climb stone walls
     * @param ac the character to interact with
     */
    @Override
    public void process(ActiveCharacter ac) {
        ((Character) ac).pickTool();
        log("The character " + ((Character) ac).getDisplay() + " has found a tool.");
    }

    @Override
    public void leave(ActiveCharacter ac) {

    }
}
