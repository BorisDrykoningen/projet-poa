import java.util.ArrayList;
import java.util.HashSet;

public class Main {
    private static final int HUNTERS_COUNT = 4;
    private static final int TOOLS_COUNT = 2;
    private static final int WALLS_COUNT = 3;
    private static final int GLUES_COUNT = 2;
    private static final int ROADMAPS_COUNT = 2;
    private static final Position TOP_LEFT = new Position(1, 1);
    private static final Position BOTTOM_RIGHT = new Position(10, 10);


    private static int randint(int min, int max) {
        return (int) (Math.random() * (max - min + 1)) + min;
    }


    // We use a builder pattern to generate most Occupants (except Walls)
    interface OccupantGenerator {
        Occupant newOccupant(Position pos, Board board);
    }


    static class TreasureGenerator implements OccupantGenerator {
        @Override
        public Occupant newOccupant(Position pos, Board board) {
            Treasure t = new Treasure(pos, board);
            board.setTreasure(t);
            return t;
        }
    }


    static class ToolsGenerator implements OccupantGenerator {
        @Override
        public Occupant newOccupant(Position pos, Board board) {
            return new Tool(pos, board);
        }
    }


    static class GluesGenerator implements OccupantGenerator {
        @Override
        public Occupant newOccupant(Position pos, Board board) {
            return new Glue(pos, board);
        }
    }


    static class RoadmapsGenerator implements OccupantGenerator {
        @Override
        public Occupant newOccupant(Position pos, Board board) {
            return new RoadMap(pos, board);
        }
    }


    static class HuntersGenerator implements OccupantGenerator {
        @Override
        public Occupant newOccupant(Position pos, Board board) {
            return new Hunter(pos, board);
        }
    }


    private static Occupant triviallyPlaceObject(Board board, OccupantGenerator gen) {
        Position pos;
        do {
            pos = new Position(
                randint(TOP_LEFT.getX(), BOTTOM_RIGHT.getX()),
                randint(TOP_LEFT.getY(), BOTTOM_RIGHT.getY())
            );
        } while (board.getCellOccupants(pos) != null);

        return gen.newOccupant(pos, board);
    }


    private static void triviallyPlaceObjects(Board board, OccupantGenerator gen, int n) {
        for (int i = 0; i < n; ++i) {
            triviallyPlaceObject(board, gen);
        }
    }


    private static ArrayList<ActiveCharacter> generateMap(Board board) {
        HashSet<Position> availableCells = new HashSet<Position>();
        for (int x = TOP_LEFT.getX(); x <= BOTTOM_RIGHT.getX(); ++x) {
            for (int y = TOP_LEFT.getY(); y <= BOTTOM_RIGHT.getY(); ++y) {
                availableCells.add(new Position(x, y));
            }
        }

        // Create walls (the hardest part goes first)
        int i = 0;
        while (i < WALLS_COUNT) {

            boolean isVertical = Math.random() < 0.5;
            // Either from top to bottom, or from left to right
            int direction = isVertical ? 3 : 1;

            // The start has no stone associated to it
            Position start = new Position(
                randint(TOP_LEFT.getX() + 1, BOTTOM_RIGHT.getX() - 1),
                randint(TOP_LEFT.getY() + 1, BOTTOM_RIGHT.getY() - 1)
            );
            // And for a wall to exist, there should be at least 2 aligned
            // stones, which means 4 aligned empty cells
            Position next = start;
            // When all the conditions are fulfilled, we get a planning
            // permission
            int maxLength = 0;
            while (availableCells.contains(next)) {
                ++maxLength;
                next = next.step(direction);
            }
            if (maxLength < 4) {
                continue;
            }
            // The maximum length is the number of aligned cells minus 2 because
            // we need some space for the edges of the wall
            maxLength -= 2;
            int length = randint(2, maxLength);

            int myLeft;
            int myRight;
            if (isVertical) {
                myLeft = 1;
                myRight = 5;
            } else {
                myLeft = 7;
                myRight = 3;
            }

            // We have the planning permission! We can start building
            ArrayList<Stone> stones = new ArrayList<Stone>();
            assert availableCells.contains(start);
            availableCells.remove(start);
            Position curr = start.step(direction);
            for (int j = 0; j < length; ++j) {
                assert availableCells.contains(curr);
                availableCells.remove(curr);
                availableCells.remove(curr.step(myLeft));
                availableCells.remove(curr.step(myRight));
                // We create a new Stone that inserts itself in the board
                stones.add(new Stone(curr, board));
                curr = curr.step(direction);
            }
            assert availableCells.contains(curr);
            availableCells.remove(curr);

            new Wall(stones);

            // We have built a wall, signaling it (Trump would be proud of us)
            ++i;
        }

        // We no longer need that, as only walls may fill a place they aren't on
        availableCells = null;

        // THERE BE TREASURE
        triviallyPlaceObject(board, new TreasureGenerator());
        triviallyPlaceObjects(board, new ToolsGenerator(), TOOLS_COUNT);
        triviallyPlaceObjects(board, new GluesGenerator(), GLUES_COUNT);
        triviallyPlaceObjects(board, new RoadmapsGenerator(), ROADMAPS_COUNT);

        HuntersGenerator gen = new HuntersGenerator();
        ArrayList<ActiveCharacter> hunters = new ArrayList<ActiveCharacter>();
        for (i = 0; i < HUNTERS_COUNT; ++i) {
            hunters.add((ActiveCharacter) triviallyPlaceObject(board, gen));
        }
        return hunters;
        }

    public static void main(String[] args) {
        Board board = new Board(BOTTOM_RIGHT.getX() + 2, BOTTOM_RIGHT.getY() + 2);
        board.generateEmptyBoard();
        ArrayList<ActiveCharacter> characters = generateMap(board);
        Model model = new Model(board, characters);
        GuiView view = new GuiView(model);
        GuiController ctl = new GuiController(model, view);

        view.update();
    }
}
