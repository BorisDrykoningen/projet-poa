public class Glue extends Occupant {
    /**
     * Creates a new Glue piece
     *
     * @param pos     the occupant's position
     * @param board   the game's board
     */
    public Glue(Position pos, Board board) {
        super(pos, board, '~');
    }

    /**
     * Makes the character wait one turn
     * @param ac the character to interact with
     */
    @Override
    public void process(ActiveCharacter ac) {
        ((Character)ac).setPos(this.getPos());
        ac.wait(2);
        log("The character " + ((Character) ac).getDisplay() + " has been glued on the case and has to wait two turn.");
    }

    @Override
    public void leave(ActiveCharacter ac) {
        // Nothing special to do
    }
}
