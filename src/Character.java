import java.util.ArrayList;
import java.util.List;

/**
 * A character that is movable
 */
public abstract class Character extends Occupant implements ActiveCharacter {

    private int direction;
    /**
     * How many turns shall we go in direction instead of computing it from the
     * top of dirStack?
    */
    private int forceDirectionFor;
    private boolean tool;
    private ArrayList<Position> dirStack;



    /**
     * Creates a new Character with a random direction
     * @param pos the character's position
     * @param board the game's board
     */
    public Character(Position pos, Board board, char display) {
        super(pos, board, display);
        tool = false;
        dirStack = new ArrayList<Position>();
        setRandomDirection();
        forceDirectionFor = 0;
    }

    /**
     * Set a random direction between 1 and 8
     */
    public void setRandomDirection() {
        direct((int) ((Math.random() * 8)+1));
    }

    /**
     * Return the current direction of the character
     * @return the direction
     */
    @Override
    public int currentDirection() {
        assert direction >= 1;
        assert direction <= 8;

        // We may need to update the stack
        if (!dirStack.isEmpty() && dirStack.get(dirStack.size() - 1).equals(getPos())) {
            dirStack.remove(dirStack.size() - 1);
        }

        // We may need to follow the integer-based direction
        if (forceDirectionFor > 0 || dirStack.isEmpty()) {
            return direction;
        }

        // Otherwise, we compute it

        // Warning: as we use screen coordinates, the trigonometric circle is
        // flipped, and we turn clockwise around it
        Position target = dirStack.get(dirStack.size() - 1);

        int vx = target.getX() - getPos().getX();
        int vy = target.getY() - getPos().getY();

        if (Math.abs(vx) > Math.abs(vy)) {
            return vx > 0 ? 1 : 5;
        } else if (Math.abs(vy) > Math.abs(vx)) {
            return vy > 0 ? 7 : 3;
        }
        if (vx > 0) {
            return vy > 0 ? 8 : 2;
        }
        return vy > 0 ? 6 : 4;
    }

    /**
     * Changes the character's direction to the given direction
     * @param direction the new direction
     */
    @Override
    public void direct(int direction) {
        direct(direction, 3);
    }

    /**
     * Changes the character's direction to the given direction
     * @param direction the new direction
     * @param nbTurns the number of turns to follow the direction
    */
    public void direct(int direction, int nbTurns) {
        assert direction >= 1;
        assert direction <= 8;

        this.direction = direction;
        forceDirectionFor = nbTurns;
    }

    public void oneTurnElapsed() {
        if (forceDirectionFor > 0) {
            --forceDirectionFor;
        }
    }

    @Override
    public void directTo(Position p) {
        dirStack.add(p);
        forceDirectionFor = 0;
    }

    /**
     * Inform if the character has a tool
     * @return true if the character has a tool, false if not
     */
    @Override
    public boolean hasTool() {
        return tool;
    }

    @Override
    public void pickTool() {
        tool = true;
    }

    @Override
    public void useTool() {
        if (!tool) {
            throw new IllegalStateException("I have no tool!");
        }
        tool = false;
    }

    /**
     * Give or remove a tool to the character
     * @param tool true to give a tool, false to remove it
     */
    @Deprecated
    public void setTool(boolean tool) {
        this.tool = tool;
    }
}
