import java.util.List;


public class Model {
    private Board m_board;
    private List<ActiveCharacter> m_characters;
    private String m_buffer;
    private int currentPlayerIndex;

    /**
     * Creates a new Model
     * @param board the Board to use
     * @param characters the ActiveChararcter's to update at each turn
    */
    public Model(Board board, List<ActiveCharacter> characters) {
        m_board = board;
        m_board.setModel(this);
        m_characters = characters;
        m_buffer = new String();
        currentPlayerIndex = 0;
    }

    /**
     * Updates once each character
    */
    public void nextTurn() {
        ActiveCharacter currentPlayer = m_characters.get(currentPlayerIndex);
        currentPlayer.play();

        currentPlayerIndex++;
        if (currentPlayerIndex >= m_characters.size()) {
            currentPlayerIndex = 0;
        }
    }

    /**
     * Tells if the game is over
     * @return true if it is, false otherwise
    */
    public boolean isFinished() {
        return m_board.getTreasure().isTaken();
    }

    /**
     * Gives access to the Board
     * @return the board wrapped into this
    */
    public Board getBoard() {
        return m_board;
    }

    public List<ActiveCharacter> getM_characters() {
        return m_characters;
    }

    /**
     * Writes logs to the internal buffer
     * @param line a one-line message to write
    */
    public void log(String line) {
        m_buffer += line + '\n';
    }

    /**
     * Retreives and cleans the logs
     * @return the logs this turn
    */
    public String acquireLogs() {
        String res = m_buffer;
        m_buffer = new String();
        return res;
    }


    public int getCurrentPlayerIndex() {
        return currentPlayerIndex;
    }
}
