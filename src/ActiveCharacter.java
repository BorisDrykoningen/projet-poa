/**
 * A character that is able to move, and thus, not stay passive
*/
public interface ActiveCharacter {
    /**
     * Changes the direction of the ActiveCharacter to the given position. Once
     * they reach it, they are free to behave how they want
     * @param p the Position to move to
    */
    void directTo(Position p);

    /**
     * Changes the direction of the ActiveCharacter to the given direction. A
     * direction is definetly never reached, as opposed to a Position
     * @param direction the direction to move to. See the project's paper for
     * more infos
    */
    void direct(int direction);

    /**
     * Waits for some time before playing again
     * @param nturns the number of turns to wait for
    */
    void wait(int nturns);

    /**
     * Signals the ActiveCharacter they are required to try another action
    */
    void playAgain();

    /**
     * Gives access to the direction of the current ActiveCharacter
     * @return the direction of the ActiveCharacter
    */
    int currentDirection();

    /**
     * Tells whether the given ActiveCharacter has a tool allowing them to climb
     * walls
     * @return true if they have one, false otherwise
    */
    boolean hasTool();

    /**
     * Informs this ActiveCharacter they have picked a tool
    */
    void pickTool();

    /**
     * Informs this ActiveCharacter they have used a tool. They must already
     * have one
     * @throws IllegalStateException if this method is called when hasTool
     * returns false
    */
    void useTool();

    /**
     * Plays. The ActiveCharacter may do whatever they want
    */
    void play();

    Position getPos();
}

