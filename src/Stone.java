import java.util.List;

public class Stone extends Occupant {

    private Wall wall;

    /**
     * Creates a new Stone
     *
     * @param pos     the occupant's position
     * @param board   the game's board
     */
    public Stone(Position pos, Board board) {
        super(pos, board, '#');
    }

    public void setWall(Wall wall) {
        this.wall = wall;
    }

    /**
     * If the character doesn't have a tool, direct the character towards the closest side of the wall
     * If the character has a tool, does not change the direction
     * @param ac the character to interact with
     */
    @Override
    public void process(ActiveCharacter ac) {
        if (ac.hasTool()) {
            log("The character " + ((Character)ac).getDisplay() + " has used their tool and has climbed on the wall.");
        } else {
            // Find the closest edge
            Position posA = wall.sideA();
            Position posB = wall.sideB();

            int sqDistA = ((Occupant)ac).getDistanceFrom(posA);
            int sqDistB = ((Occupant)ac).getDistanceFrom(posB);
            if (sqDistA == 0 || sqDistB == 0) {
                ((Character) ac).setRandomDirection();
                ac.playAgain();
            } else {
                ac.directTo(sqDistA > sqDistB ? posB : posA);
                ac.playAgain();
            }
        }
    }

    /**
     * Destroy the character's tool once they leave the wall
     * @param ac the character to interact with
     */
    @Override
    public void leave(ActiveCharacter ac) {
        Position newPos = getPos().step(ac.currentDirection());

        boolean newPosFound = false;
        for (Stone i: wall.getStones()) {
            if (i.getPos().equals(newPos)) {
                newPosFound = true;
                break;
            }
        }

        if (!newPosFound) {
            log("The character " + ((Character) ac).getDisplay() + " have lost their tool");
            if (ac.hasTool()) {
                ac.useTool();
            } else {
                log("The character has no tool! This is a bug and should be reported");
            }
        }
    }

}
