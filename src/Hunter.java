import java.util.List;

public class Hunter extends Character {

    private static int instanceNumber = 0;
    private boolean shouldPlayAgain;
    private int waitFor;

    /**
     * Creates a new hunter
     * @param pos the hunter's position
     * @param board the game's board
     */
    public Hunter(Position pos, Board board) {
        super(pos, board, (char) ('A' + instanceNumber));
        // Don't care
        shouldPlayAgain = false;
        waitFor = 0;
        instanceNumber++;
    }

    @Override
    public void wait(int nturns) {
        waitFor += nturns;
    }

    /**
     * Set a random direction to the character ac
     * @param ac the character to interact with
     */
    @Override
    public void process(ActiveCharacter ac) {
        ((Character)ac).setRandomDirection();
        ac.playAgain();
        log("The character " + ((Character) ac).getDisplay() + " has been given a new random direction.");
    }

    @Override
    public void leave(ActiveCharacter ac) {}

    @Override
    public void playAgain() {
        shouldPlayAgain = true;
    }

    @Override
    public void play() {
        if (waitFor > 0) {
            --waitFor;
            log("The character " + getDisplay() + " can't play during this turn.");
        } else {
            shouldPlayAgain = true;
            while (shouldPlayAgain) {
                shouldPlayAgain = false;
                moveToTargetPosition();
            }
        }

        oneTurnElapsed();
    }

    /**
     * Inform if the target cell position is empty or not
     * @return true if the cell is empty, false if not
     */
    public boolean isTargetPositionEmpty() {
        Position target = this.getPos().step(currentDirection());
        return (this.getBoard().getCellOccupants(target) == null);
    }

    /**
     * Attempt to move the target to the target position.
     * If the cell is empty, succeed.
     * If the cell is not empty, the character's behavior changes according to the kind of Occupant on the cell
     */
    public void moveToTargetPosition() {
        Position target = this.getPos().step(currentDirection());
        List<Occupant> list = this.getBoard().getCellOccupants(target);
        // We process the whole stack as long as nobody tells us our move is
        // invalid and we should try something else
        if (list != null) {
            for (int i = list.size() - 1; i >= 0 && !shouldPlayAgain; --i) {
                list.get(i).process(this);
            }
        }
        if (shouldPlayAgain) {
            return;
        }

        // Once the move is validated, we leave the current cell
        for (Occupant i: getBoard().getCellOccupants(getPos())) {
            if (i != this) {
                i.leave(this);
            }
        }

        // We have the right to move
        if (!shouldPlayAgain) {
            setPos(target);
        }
    }
}
