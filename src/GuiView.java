import java.util.List;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.GridLayout;
import java.util.Objects;


public class GuiView extends JFrame {
    private Model m_model;
    // Where the board stuff goes
    private JPanel m_boardPanel;
    // Where the messages and the button go
    private JPanel m_interfacePanel;
    // The button
    private JButton m_updateButton;
    // The message zone
    private JTextArea m_messages;
    private JScrollPane m_scrollPane;

    /**
     * Creates a new GuiView
     * @param model the Model to view
    */
    public GuiView(Model model) {
        super();

        m_model = model;

        setVisible(true);
        setSize(800,800);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        Container win = getContentPane();
        win.setLayout(new BorderLayout());

        m_boardPanel = new JPanel();
        win.add(m_boardPanel, BorderLayout.CENTER);
        m_boardPanel.setLayout(new GridLayout(m_model.getBoard().getX(), m_model.getBoard().getY()));

        m_interfacePanel = new JPanel();
        win.add(m_interfacePanel, BorderLayout.SOUTH);

        m_messages = new JTextArea();
        m_messages.setEditable(false);
        m_messages.setColumns(50);
        m_messages.setRows(5);
        m_messages.setVisible(true);
        m_scrollPane = new JScrollPane(m_messages);
        m_interfacePanel.add(m_scrollPane);

        m_scrollPane.revalidate();
        m_scrollPane.repaint();

        m_updateButton = new JButton("Tour suivant");
        m_interfacePanel.add(m_updateButton);

        update();
    }


    /**
     * Updates the view, according to the model
    */
    public void update() {
        Board b = m_model.getBoard();
        m_boardPanel.removeAll();
        for (int i = 0; i < b.getY(); ++i) {
            for (int j = 0; j < b.getX(); ++j) {
                Position p = new Position(j, i);
                char display;
                List<Occupant> occs = b.getCellOccupants(p);
                if (occs != null) {
                    Occupant top = occs.get(occs.size() - 1);
                    display = top.getDisplay();
                } else {

                    display = 'w';
                }
                JLabel a = new JLabel();
                ImageIcon image = new ImageIcon(Objects.requireNonNull(getClass().getResource("images/" + display + ".png")));
                a.setIcon(image);
                //a.setText(String.valueOf(display));
                m_boardPanel.add(a);
            }
        }


        for (ActiveCharacter character : m_model.getM_characters()) {
            Position characterPos = character.getPos();
            int labelIndex = characterPos.getY() * m_model.getBoard().getX() + characterPos.getX();
            JLabel label = (JLabel) m_boardPanel.getComponent(labelIndex);
            //label.setText(String.valueOf(((Occupant)character).getDisplay()));
        }

        m_messages.append(m_model.acquireLogs());

        if (m_model.isFinished()) {
            m_updateButton.setEnabled(false);
        }

        revalidate();
        repaint();
    }

    /**
     * Allows to listen for clicks to the "next turn" button
     * @param lst the listener to signal when the button is clicked
    */
    void onNextTurn(ActionListener lst) {
        m_updateButton.addActionListener(lst);
    }
}
