/**
 * An Occupant that can be questionned about interractions with them
*/
public interface Questionnable {
    /**
     * Interacts with the given ActiveCharacter when they come to the same tile
     * as this
     * @param ac the character to interact with
    */
    void process(ActiveCharacter ac);

    /**
     * Interacts with the given ActiveCharacter when they leave the current tile
     * @param ac the character to interact with
    */
    void leave(ActiveCharacter ac);
}
