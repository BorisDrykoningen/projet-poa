import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class GuiController {
    private Model m_model;
    private GuiView m_view;

    private class NextTurnListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (!m_model.isFinished()) {
                m_model.nextTurn();
                m_view.update();
            }
        }
    }

    public GuiController(Model model, GuiView view) {
        m_model = model;
        m_view = view;
        m_view.onNextTurn(new NextTurnListener());
    }
}
