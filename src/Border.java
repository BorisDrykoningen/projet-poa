public class Border extends Occupant {
    /**
     * Creates a new Occupant
     *
     * @param pos     the occupant's position
     * @param board   the game's board
     */
    public Border(Position pos, Board board) {
        super(pos, board, '(');
        if (pos.getY() == 0 || pos.getY() == board.getY() - 1) {
            this.setDisplay('_');
        }
    }

    /**
     * Changes the character's direction to the opposite of their current direction
     * @param ac the character to interact with
     */
    @Override
    public void process(ActiveCharacter ac) {
        int oldDir = ac.currentDirection();
        int dir;
        if (getPos().getY() == 0 || getPos().getY() == getBoard().getY() - 1) {
            // Horizontal bouncing. The formula below is pure black magic, it
            // has been found empirically
            dir = 10 - oldDir;
        } else {
            // Vertical bouncing. The formula below is pure black magic, it has
            // been found empirically
            dir = (13 - oldDir) % 8 + 1;
        }

        ac.direct(dir);
        ac.playAgain();
    }

    @Override
    public void leave(ActiveCharacter ac) {
        throw new IllegalStateException("How the hell are we supposed to leave a cell we can't go on?");
    }
}
