import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 * The game's board
*/
public class Board {
    private HashMap<Position, List<Occupant>> m_board;
    private Treasure m_treasure;
    private int x, y;
    private Model m_parent;

    /**
     * Creates an empty board
    */
    public Board(int x, int y) {
        m_board = new HashMap<Position, List<Occupant>>();
        this.x = x;
        this. y = y;
    }

    /**
     * Deletes an Occupant from a cell
     * @param pos the Position of the Occupant
     * @param ref a reference to the Occupant to delete. It must be on that cell
    */
    public void removeFromCell(Position pos, Occupant ref) {
        List<Occupant> ocs = m_board.get(pos);
        if (ocs.size() == 1) {
            if (ocs.get(0) != ref) {
                throw new IllegalArgumentException("ref not on cell pos");
            }
            m_board.remove(pos);
            return;
        }

        for (int i = 0; i < ocs.size(); ++i) {
            if (ocs.get(i) == ref) {
                ocs.remove(i);
                return;
            }
        }

        throw new IllegalArgumentException("ref not on cell pos");
    }

    /**
     * Adds an Occupant on the top of a cell. The Occupant is always added, so
     * make sure it has been allowed by other Occupants to come in
     * @param pos the Position of the Occupant
     * @param occ the Occupant to add
    */
    public void addToCell(Position pos, Occupant occ) {
        List<Occupant> others = m_board.get(pos);
        if (others == null) {
            others = new ArrayList<Occupant>();
            others.add(occ);
            m_board.put(pos, others);
        } else {
            others.add(occ);
        }
    }

    /**
     * Gets the content of the given cell
     * @param pos the position to get the content
     * @return a list of the content. The Occupant on the top of the stack has
     * the greater index, then goes the second, then the third, etc... returns a
     * null reference if the cell has no content
    */
    public List<Occupant> getCellOccupants(Position pos) {
        return m_board.get(pos);
    }


    /**
     * Create an empty board with only borders
     */
    public void generateEmptyBoard() {
        for (int i = 0; i < this.y; i++) {
            Position p1 = new Position(0, i);
            Position p2 = new Position(this.x - 1, i);
            addToCell(p1, new Border(p1, this));
            addToCell(p2, new Border(p2, this));
        }
        for (int i = 0; i < this.x; i++) {
            Position p1 = new Position(i, 0);
            Position p2 = new Position(i, this.y - 1);
            addToCell(p1, new Border(p1, this));
            addToCell(p2, new Border(p2, this));
        }
    }


    /**
     * Gives access to the last added Occupant on a cell
     * @param pos the position of the cell
     * @return the Occupant on the cell
     */
    public Occupant getHighestCellOccupant(Position pos) {
        return getCellOccupants(pos).get(getCellOccupants(pos).size() - 1);
    }


    /**
     * Display the game's board according to the Occupant on each cell
     */
    public void displayBoard() {
        for (int j = 0; j < this.y; j++) {
            for (int i = 0; i < this.x; i++) {
                Position pos = new Position(i, j);
                if (this.getCellOccupants(pos) == null) {
                    System.out.print(". ");
                } else {
                    System.out.print(getHighestCellOccupant(pos) + " ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Gives access to the abscissa
     * @return the abscissa
     */
    public int getX() {
        return x;
    }

    /**
     * Gives access to the ordinate
     * @return the ordinate
     */
    public int getY() {
        return y;
    }

    /**
     * Gives access to the treasure
     * @return the treasure
    */
    public Treasure getTreasure() {
        return m_treasure;
    }

    /**
     * Caches the treasure
     * @param treasure the treasure. Must be placed on the Board
    */
    public void setTreasure(Treasure treasure) {
        m_treasure = treasure;
    }

    /**
     * Gives the treasure's position
     * @return the treasure's position
    */
    public Position getTreasurePosition() {
        return m_treasure.getPos();
    }

    /**
     * Sets the parent model
     * @param model the parent model
    */
    public void setModel(Model model) {
        m_parent = model;
    }

    /**
     * Logs a message. Will be sent to the parent model
     * @param line the line to log
     * @see setModel
    */
    public void log(String line) {
        m_parent.log(line);
    }
}

