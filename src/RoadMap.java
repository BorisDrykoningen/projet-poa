public class RoadMap extends Occupant implements Questionnable{
    /**
     * Creates a new RoadMap
     *
     * @param pos     the roadmap's position
     * @param board   the game's board
     */
    public RoadMap(Position pos, Board board) {
        super(pos, board, 'm');
    }

    /**
     * Direct the character towards the treasure
     * @param ac the character to interact with
     */
    @Override
    public void process(ActiveCharacter ac) {
        ((Character)ac).setPos(getPos());
        ac.directTo(((Character)ac).getBoard().getTreasurePosition());
        log("The hunter " + ((Character) ac).getDisplay() + " has found a map and is now heading to the treasure!");
    }

    @Override
    public void leave(ActiveCharacter ac) {

    }
}
