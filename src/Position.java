import java.lang.Comparable;


/**
 * A position in screen coordinates. This means the abscissas' axis goes from
 * left to right and the ordinates' axis goes from top to bottom
*/
public class Position implements Comparable<Position> {
    /// The abscissa
    private int m_x;

    /// The ordinate
    private int m_y;

    /**
     * Creates a new Position
     * @param x the absissa
     * @param y the ordinate
    */
    public Position(int x, int y) {
        m_x = x;
        m_y = y;
    }

    /**
     * Gives access to the abscissa
     * @return the abscissa
    */
    public int getX() {
        return m_x;
    }

    /**
     * Gives access to the ordinate
     * @return the ordinate
    */
    public int getY() {
        return m_y;
    }

    /**
     * Goes one step in the given direction
     * @param direction the direction to go to
     * @return the Position after stepping
    */
    public Position step(int direction) {
        int[] offsetX = { 1,  1,  0, -1, -1, -1,  0,  1};
        int[] offsetY = { 0, -1, -1, -1,  0,  1,  1,  1};

        return new Position(
            m_x + offsetX[direction - 1],
            m_y + offsetY[direction - 1]
        );
    }

    @Override
    public int hashCode() {
        // x and y XORed together, with some warp applied to x to prevent things
        // such as (a,b) and (b,a) to produce the same hash code. That way, the
        // elements that produce the same hash code may only exist if the map is
        // large enough
        return (((m_x & 0xffff) << 16) | ((m_x & 0xffff0000) >> 16)) ^ m_y;
    }

    // This is meant to speed-up HashMap s, not to actually make sense
    @Override
    public int compareTo(Position p) {
        // We don't need to care about integer over/underflows as they are
        // defined by Java. As we just need to return some value, we don't care
        // whether we over/underflow or not
        if (m_x != p.m_x) {
            return m_x - p.m_x;
        }
        return m_y - p.m_y;
    }

    // Needed for HashMap's to work as well
    @Override
    public boolean equals(Object o) {
        if (o.getClass() != getClass()) {
            return false;
        }
        Position p = (Position) o;
        return p.m_x == m_x && p.m_y == m_y;
    }
}

