public class Treasure extends Occupant {
    private boolean taken;

    /**
     * Creates a new Treasure
     *
     * @param pos     the occupant's position
     * @param board   the game's board
     */
    public Treasure(Position pos, Board board) {
        super(pos, board, '$');
        taken = false;
    }

    /**
     * Ends the game
     * @param ac the character that founds the treasure (MVP)
     */
    @Override
    public void process(ActiveCharacter ac) {
        log("The treasure has been found by " + ((Character) ac).getDisplay());
        taken = true;
    }

    @Override
    public void leave(ActiveCharacter ac) {

    }

    /**
     * Tells whether the Treasure has been taken
     * @return true if it has been taken, false otherwise
    */
    public boolean isTaken() {
        return taken;
    }
}
