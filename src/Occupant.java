/**
 * A character or a fixed piece set on the board
 */
public abstract class Occupant implements Questionnable {

    private Position pos;
    private Board board;
    private char display;

    /**
     * Creates a new Occupant on the Board
     * @param pos the occupant's position
     * @param board the game's board
     */
    public Occupant(Position pos, Board board, char display) {
        this.pos = pos;
        this.board = board;
        this.display = display;

        board.addToCell(pos, this);
    }

    /**
     * Give the distance between the current position and the target Occupant
     * @param pos the target position
     * @return the distance between the current position and the target position
     */
    public int getDistanceFrom(Position pos) {
        int diffX = this.pos.getX() - pos.getX();
        int diffY = this.pos.getY() - pos.getY();
        return diffX * diffX + diffY * diffY;
    }

    public Position getPos() {
        return pos;
    }

    public void setPos(Position pos) {
        board.removeFromCell(this.pos, this);
        this.pos = pos;
        board.addToCell(pos, this);
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public char getDisplay() {
        return display;
    }



    public void setDisplay(char display) {
        this.display = display;
    }

    public String toString() {
        return String.valueOf(display);
    }

    /**
     * Logs a message. Will be sent to the Board's parent model
     * @param line the line to log
     * @see setModel
    */
    public void log(String line) {
        board.log(line);
    }
}
